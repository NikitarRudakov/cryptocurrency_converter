package configs

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
	"github.com/subosito/gotenv"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/pkg/logging"
)

type Config struct {
	DSN   string
	Token string
}

func GetConfig() (*Config, error) {

	logger := logging.GetLogger()
	logger.Info("Reading application configuration")
	envPath := filepath.Join(".env")
	err := LoadEnvVariables(envPath)
	if err != nil {
		logger.Error("failed to load environment variables")
		return nil, err
	}
	storageHost := os.Getenv("STORAGE_HOST")
	storagePort := "5432" //os.Getenv("STORAGE_PORT")
	storageUser := os.Getenv("STORAGE_USER")
	storagePassword := os.Getenv("STORAGE_PASSWORD")
	storageName := os.Getenv("STORAGE_NAME")
	storageType := os.Getenv("STORAGE_TYPE")
	dsn := fmt.Sprintf("%s://%s:%s@%s:%s/%s?sslmode=disable", storageType, storageUser, storagePassword, storageHost, storagePort, storageName)
	token := os.Getenv("BOT_TOKEN")
	return &Config{
		DSN:   dsn,
		Token: token,
	}, nil
}

// GenerateURL generates a URL for the request based on the passed cryptocurrencies, currencies, and API key.
func GenerateURL(cryptos string, currencies []string, apiKey string) string {
	url := fmt.Sprintf("https://min-api.cryptocompare.com/data/pricemulti?fsyms=%s&tsyms=%s&api_key=%s",
		cryptos,
		strings.Join(currencies, ","),
		apiKey,
	)
	return url
}

// LoadEnvVariables loads environment variables from the .env file at the specified path.
func LoadEnvVariables(envPath string) error {
	logger := logging.GetLogger()
	err := gotenv.Load(envPath)
	if err != nil {
		logger.Errorf("Failed to load environment variables from file: %v", err)
		return errors.Wrapf(err, "failed to load environment variables from file: %v", err)
	}
	logger.Infof("Environment variables loaded from %s", envPath)
	return nil
}
