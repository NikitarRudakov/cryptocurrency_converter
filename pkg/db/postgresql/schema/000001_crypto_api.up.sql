CREATE TABLE rates(
    title VARCHAR(255) NOT NULL,
    price DECIMAL(20, 2) NOT NULL,
    last_view_time TIMESTAMP NOT NULL
);

CREATE TABLE currencies(
    coin VARCHAR(255) NOT NULL PRIMARY KEY
);

INSERT INTO currencies(coin) VALUES ('BTC');

INSERT INTO currencies(coin) VALUES ('ETH');
