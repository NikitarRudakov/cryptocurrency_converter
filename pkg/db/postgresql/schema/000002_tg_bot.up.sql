CREATE TABLE users_crypto (
    id SERIAL PRIMARY KEY,
    chat_id BIGINT NOT NULL,
    currencies JSONB NOT NULL DEFAULT '[]'
);

