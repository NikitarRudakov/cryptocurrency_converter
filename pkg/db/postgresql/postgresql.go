package postgresql

import (
	"context"
	"log"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/pkg/configs"
	repeatable "gitlab.com/NikitarRudakov/cryptocurrency_converter/pkg/utils"
)

func NewClient(ctx context.Context, maxAttempts int, config *configs.Config) (pool *pgxpool.Pool, err error) {
	if err != nil {
		return nil, err
	}
	err = repeatable.DoWithTries(func() error {
		ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
		defer cancel()

		pool, err = pgxpool.Connect(ctx, config.DSN)
		if err != nil {
			return err
		}

		return nil
	}, maxAttempts, 5*time.Second)

	if err != nil {
		log.Fatalf("error do with tries postgresql: %v", err)

	}

	return pool, nil
}

type PostgreSQLClient struct {
	Pool *pgxpool.Pool
}
