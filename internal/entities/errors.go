package entities

import "errors"

var (
	ErrInvalidParam    = errors.New("invalid param")
	ErrIncorrectType   = errors.New("incorrect data type")
	ErrNonExistentURL  = errors.New("non-existent URL")
	ErrSaveFailed      = errors.New("save failed")
	ErrGetCourse       = errors.New("failed get cryptocurrency course")
	ErrStorageInitFail = errors.New("storage initialization failed")
	ErrStorageRead     = errors.New("storage read error")
)
