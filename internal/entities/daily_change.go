package entities

type DailyChange struct {
	Currency      string
	MaxValue      float64
	MinValue      float64
	PercentChange float64
}

func NewDailyChange(currency string, minValue, maxValue, percentChange float64) *DailyChange {
	return &DailyChange{
		Currency:      currency,
		MaxValue:      maxValue,
		MinValue:      minValue,
		PercentChange: percentChange,
	}
}
