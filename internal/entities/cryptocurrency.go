package entities

import "time"

type Cryptocurrency struct {
	Title        string
	Price        float64
	LastViewTime time.Time
}

func NewCryptocurrency(title string, price float64, lastViewTime time.Time) *Cryptocurrency {
	return &Cryptocurrency{
		Title:        title,
		Price:        price,
		LastViewTime: lastViewTime,
	}
}
