package adapters

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/entities"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/pkg/configs"
)

type ScrapperCryptoCompare struct {
	logger *logrus.Logger
}

func NewScrapperCryptoCompare(logger *logrus.Logger) (*ScrapperCryptoCompare, error) {
	if logger == nil {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "logger init failed: %v", logger)
	}
	return &ScrapperCryptoCompare{
		logger: logger,
	}, nil
}

func (s *ScrapperCryptoCompare) GetURLScrapper(ctx context.Context, cryptos string) (string, error) {
	envPath := filepath.Join(".env")
	err := configs.LoadEnvVariables(envPath)
	if err != nil {
		s.logger.Error("failed to load environment variables")
		return "", err
	}
	currencies := strings.Split(os.Getenv("CURRENCIES"), ",")
	apiKey := os.Getenv("API_KEY")

	url := configs.GenerateURL(cryptos, currencies, apiKey)
	return url, nil
}

func (s *ScrapperCryptoCompare) GetCourse(ctx context.Context, cryptos string) (*entities.Cryptocurrency, error) {
	url, err := s.GetURLScrapper(ctx, cryptos)
	if err != nil {
		s.logger.Errorf("Failed to get URL: %v", err)
		return nil, errors.Wrap(err, "Failed to get URL")
	}

	response, err := http.Get(url)
	if err != nil {
		s.logger.Errorf("Failed to get data from URL: %v", err)
		return nil, errors.Wrap(err, "Failed to get data from URL")
	}
	defer response.Body.Close()

	var data map[string]map[string]float64
	if err := json.NewDecoder(response.Body).Decode(&data); err != nil {
		s.logger.Errorf("Failed to decode JSON: %v", err)
		return nil, errors.Wrap(err, "Failed to decode JSON")
	}

	// Проверяем, есть ли данные для указанной криптовалюты
	priceData, ok := data[cryptos]
	if !ok {
		return nil, errors.New("Data for the specified cryptocurrency not found")
	}

	// Извлекаем данные только для валюты USD
	priceUSD, ok := priceData["USD"]
	if !ok {
		return nil, errors.New("USD data not found for the specified cryptocurrency")
	}
	fmt.Println(cryptos, priceUSD)
	// Создаем объект Cryptocurrency для указанной криптовалюты
	cryptocurrency := entities.NewCryptocurrency(cryptos, priceUSD, time.Now())
	return cryptocurrency, nil
}
