package adapters

import (
	"context"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/entities"
)

type PGStorage struct {
	pgx    *pgxpool.Pool
	logger *logrus.Logger
}

func NewPGStorage(pgx *pgxpool.Pool, logger *logrus.Logger) (*PGStorage, error) {
	if pgx == nil {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "storage init failed: %v", pgx)
	}
	if logger == nil {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "logger init failed: %v", logger)
	}
	return &PGStorage{
		pgx:    pgx,
		logger: logger,
	}, nil
}

func (p *PGStorage) Save(ctx context.Context, rates []*entities.Cryptocurrency) error {
	for _, rate := range rates {
		q := `INSERT INTO rates 
		(title, price, last_view_time) 
		VALUES ($1, $2, $3);`
		_, err := p.pgx.Exec(ctx, q, rate.Title, rate.Price, rate.LastViewTime)
		if err != nil {
			p.logger.Errorf("failed to save data: %v", err)
			return errors.Wrapf(entities.ErrSaveFailed, "failed to save data: %v", err)
		}
	}
	p.logger.Info("Data saved successfully")
	return nil
}

func (p *PGStorage) GetByCurrency(ctx context.Context, currency string) (*entities.Cryptocurrency, error) {
	q := `SELECT title, price 
	FROM rates 
	WHERE title = $1;`
	row := p.pgx.QueryRow(ctx, q, currency)

	var title string
	var price float64

	if err := row.Scan(&title, &price); err != nil {
		if err == pgx.ErrNoRows {
			p.logger.Warnf("no rows found for currency: %s", currency)
			return nil, errors.Wrapf(entities.ErrStorageRead, "no rows found for currency: %s", currency)
		}
		p.logger.Errorf("failed to scan row: %v", err)
		return nil, errors.Wrapf(entities.ErrStorageRead, "failed to scan row: %v", err)
	}
	crypto := entities.NewCryptocurrency(title, price, time.Now())

	return crypto, nil
}

func (p *PGStorage) GetStatsByTitle(ctx context.Context, currency string) (*entities.DailyChange, error) {
	var min, max float64
	var percentChange float64
	q := `SELECT MIN(price), MAX(price) 
	FROM rates 
	WHERE title = $1 
	AND last_view_time >= $2;`
	row := p.pgx.QueryRow(ctx, q, currency, time.Now().Add(-24*time.Hour))

	// Scanning the result of the first query
	err := row.Scan(&min, &max)
	if err != nil {
		p.logger.Errorf("failed to query min and max values for currency: %s: %v", currency, err)
		return nil, errors.Wrapf(entities.ErrStorageRead, "failed to query min and max values for currency: %s", currency)
	}

	request := `SELECT 
    (
        (SELECT price 
         FROM rates 
         WHERE title = $1 
         AND last_view_time >= $2 
         ORDER BY last_view_time DESC LIMIT 1
        ) - (
            SELECT price 
            FROM rates 
            WHERE title = $1 
            AND last_view_time >= $3 
            ORDER BY last_view_time ASC LIMIT 1
        )
    ) / (
        SELECT price 
        FROM rates 
        WHERE title = $1 
        AND last_view_time >= $3 
        ORDER BY last_view_time ASC LIMIT 1
    ) * 100;`
	row = p.pgx.QueryRow(ctx, request, currency, time.Now().Add(-1*time.Hour), time.Now().Add(-24*time.Hour))

	// Scanning the result of the second query
	err = row.Scan(&percentChange)
	if err != nil {
		p.logger.Errorf("failed to query percent change for currency: %s: %v", currency, err)
		return nil, errors.Wrapf(entities.ErrStorageRead, "failed to query percent change for currency: %s", currency)
	}

	// Creating a new daily change object and returning it
	dailyChange := entities.NewDailyChange(currency, min, max, percentChange)
	return dailyChange, nil
}
