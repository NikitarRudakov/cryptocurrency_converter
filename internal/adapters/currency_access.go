package adapters

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/entities"
)

func (p *PGStorage) AddCurrency(ctx context.Context, currency []string) error {
	// Prepare the query for inserting into the currencies table
	qCurrency := `INSERT INTO currencies
	(coin) 
	VALUES ($1);`

	// Loop through the list of currencies to add them to the table
	for _, coin := range currency {
		// Execute the query to insert the currency into the currencies table
		_, err := p.pgx.Exec(ctx, qCurrency, coin)
		if err != nil {
			// Log and return an error if there is a problem inserting the currency
			p.logger.Errorf("Error adding currency %s: %v", coin, err)
			return errors.Wrapf(entities.ErrSaveFailed, "Error adding currency %s: %v", coin, err)
		}
	}

	// Log success message if all currencies are added successfully
	p.logger.Info("Currencies added successfully")
	return nil
}

func (p *PGStorage) GetCurrencies(ctx context.Context) ([]string, error) {
	// Prepare the query to fetch all currencies from the currencies table
	q := `SELECT coin 
	FROM currencies;`
	rows, err := p.pgx.Query(ctx, q)
	if err != nil {
		p.logger.Errorf("Failed to fetch currencies: %v", err)
		return nil, errors.Wrap(entities.ErrStorageRead, "Failed to fetch currencies")
	}
	defer rows.Close()

	// Initialize a slice to store the currencies
	var currencies []string

	// Iterate over the rows and append currencies to the slice
	for rows.Next() {
		var coin string
		if err := rows.Scan(&coin); err != nil {
			p.logger.Errorf("Failed to scan row: %v", err)
			return nil, errors.Wrap(entities.ErrStorageRead, "Failed to scan row")
		}
		currencies = append(currencies, coin)
	}
	if err := rows.Err(); err != nil {
		p.logger.Errorf("Error in iterating rows: %v", err)
		return nil, errors.Wrap(entities.ErrStorageRead, "Error in iterating rows")
	}

	return currencies, nil
}
