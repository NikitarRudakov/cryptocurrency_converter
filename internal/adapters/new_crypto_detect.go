package adapters

import (
	"context"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/cases"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/entities"
)

type CryptoDetect struct {
	storage  cases.Storage
	scrapper cases.Scrapper
	logger   *logrus.Logger
}

func NewCryptoDetect(storage cases.Storage, scrapper cases.Scrapper, logger *logrus.Logger) (*CryptoDetect, error) {
	if storage == nil || storage == cases.Storage(nil) {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "new service storage init failid: %v", storage)
	}
	if scrapper == nil || scrapper == cases.Scrapper(nil) {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "scrapper init failed: %v", scrapper)
	}
	if logger == nil {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "logger init failed: %v", logger)
	}
	return &CryptoDetect{
		storage:  storage,
		scrapper: scrapper,
		logger:   logger,
	}, nil
}

func (s *CryptoDetect) ProcessMissingCoins(ctx context.Context, coins []string) error {
	cryptoCoins, errGetCoins := s.storage.GetCurrencies(ctx)
	if errGetCoins != nil {
		s.logger.Errorf("Failed to get coins: %v", errGetCoins)
		return errors.Wrapf(entities.ErrStorageInitFail, "Failed to get coins: %v", errGetCoins)
	}
	var rates []*entities.Cryptocurrency
	seen := make(map[string]struct{})

	// Заполняем карту видимости криптовалютами из cryptoCoins
	for _, v := range cryptoCoins {
		seen[v] = struct{}{}
	}

	var cryptocurrencyes []string

	// Находим отсутствующие криптовалюты из coins
	for _, v := range coins {
		if _, ok := seen[v]; !ok {
			cryptocurrencyes = append(cryptocurrencyes, v)
		}
	}
	var successfulCryptocurrencyNames []string
	for _, currency := range cryptocurrencyes {
		rate, err := s.scrapper.GetCourse(ctx, currency)
		if err != nil {
			s.logger.Warnf("Failed to get rate for %s: %v", currency, err)
			continue
		}

		// Создаем новую криптовалюту
		crypto := &entities.Cryptocurrency{
			Title:        rate.Title,
			Price:        rate.Price,
			LastViewTime: rate.LastViewTime,
		}

		// Добавляем криптовалюту в список курсов
		rates = append(rates, crypto)
		successfulCryptocurrencyNames = append(successfulCryptocurrencyNames, currency)
	}

	// Сохраняем все курсы валют, если есть что сохранять
	if len(rates) > 0 {
		err := s.storage.Save(ctx, rates)
		if err != nil {
			s.logger.Errorf("Failed to save data: %v", err)
			return errors.Wrapf(entities.ErrStorageInitFail, "Failed to save data: %v", err)
		}
	}

	// Добавляем валюты только если получили успешные данные
	if len(rates) > 0 {
		err := s.storage.AddCurrency(ctx, successfulCryptocurrencyNames)
		if err != nil {
			s.logger.Errorf("Failed to add missing currencies: %v", err)
			return errors.Wrapf(entities.ErrStorageInitFail, "Failed to add missing currencies: %v", err)
		}
	}
	return nil
}
