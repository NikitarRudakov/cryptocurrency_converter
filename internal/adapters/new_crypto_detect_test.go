package adapters

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/cases"
	mock_cases "gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/cases/mocks"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/entities"
)

func TestNewCryptoDetect(t *testing.T) {
	testTable := []struct {
		name                 string
		storage              cases.Storage
		scrapper             cases.Scrapper
		logger               *logrus.Logger
		expectedErrorMessage string
	}{
		{
			name:     "Valid parameters",
			storage:  &mock_cases.MockStorage{},
			scrapper: &mock_cases.MockScrapper{},
			logger:   logrus.New(),
		},
		{
			name:                 "Invalid Storage",
			storage:              nil,
			expectedErrorMessage: "new service storage init failid: <nil>: invalid param",
		},
		{
			name:                 "Invalid Scrapper",
			storage:              &mock_cases.MockStorage{},
			scrapper:             nil,
			expectedErrorMessage: "scrapper init failed: <nil>: invalid param",
		},
		{
			name:                 "Invalid Logger",
			storage:              &mock_cases.MockStorage{},
			scrapper:             &mock_cases.MockScrapper{},
			logger:               nil,
			expectedErrorMessage: "logger init failed: <nil>: invalid param",
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			// Пытаемся создать новый CryptoService с заданными параметрами
			service, err := NewCryptoDetect(tt.storage, tt.scrapper, tt.logger)

			// Проверяем, соответствует ли результат ожиданиям
			if tt.expectedErrorMessage == "" {
				assert.NoError(t, err)
				assert.NotNil(t, service)
			} else {
				assert.Error(t, err)
				assert.Nil(t, service)
				assert.EqualError(t, err, tt.expectedErrorMessage)
			}
		})
	}
}

func TestProcessMissingCoins(t *testing.T) {
	testCases := []struct {
		name                 string
		cryptoCoins          []string
		coins                []string
		expectedErrorMessage string
		mockBehavior         func(storage *mock_cases.MockStorage, scrapper *mock_cases.MockScrapper)
	}{
		{
			name:        "Missing coins, successful retrieval",
			cryptoCoins: []string{"BTC"},
			coins:       []string{"BTC", "ETH"},
			mockBehavior: func(storage *mock_cases.MockStorage, scrapper *mock_cases.MockScrapper) {
				storage.EXPECT().GetCurrencies(gomock.Any()).Return([]string{"BTC"}, nil)
				scrapper.EXPECT().GetCourse(gomock.Any(), "ETH").Return(&entities.Cryptocurrency{
					Title: "ETH",
					Price: 2000.0,
				}, nil)
				storage.EXPECT().Save(gomock.Any(), gomock.Any()).Return(nil)
				storage.EXPECT().AddCurrency(gomock.Any(), gomock.Any()).Return(nil)
			},
		},
		{
			name:        "Errors getting currencies",
			cryptoCoins: []string{"BTC"},
			coins:       []string{"BTC", "ETH"},
			mockBehavior: func(storage *mock_cases.MockStorage, scrapper *mock_cases.MockScrapper) {
				storage.EXPECT().GetCurrencies(gomock.Any()).Return(nil, errors.New("mocked error getting currencies"))
			},
			expectedErrorMessage: "Failed to get coins: mocked error getting currencies: storage initialization failed",
		},
		{
			name:        "Missing coins, save error",
			cryptoCoins: []string{"BTC"},
			coins:       []string{"BTC", "ETH"},
			mockBehavior: func(storage *mock_cases.MockStorage, scrapper *mock_cases.MockScrapper) {
				storage.EXPECT().GetCurrencies(gomock.Any()).Return([]string{"BTC"}, nil)
				scrapper.EXPECT().GetCourse(gomock.Any(), "ETH").Return(&entities.Cryptocurrency{
					Title: "ETH",
					Price: 2000.0,
				}, nil)
				storage.EXPECT().Save(gomock.Any(), gomock.Any()).Return(errors.New("mocked save error"))
			},
			expectedErrorMessage: "Failed to save data: mocked save error: storage initialization failed",
		},
		{
			name:        "Missing coins, add currency error",
			cryptoCoins: []string{"BTC"},
			coins:       []string{"BTC", "ETH"},
			mockBehavior: func(storage *mock_cases.MockStorage, scrapper *mock_cases.MockScrapper) {
				storage.EXPECT().GetCurrencies(gomock.Any()).Return([]string{"BTC"}, nil)
				scrapper.EXPECT().GetCourse(gomock.Any(), "ETH").Return(&entities.Cryptocurrency{
					Title: "ETH",
					Price: 2000.0,
				}, nil)
				storage.EXPECT().Save(gomock.Any(), gomock.Any()).Return(nil)
				storage.EXPECT().AddCurrency(gomock.Any(), gomock.Any()).Return(errors.New("mocked add currency error"))
			},
			expectedErrorMessage: "Failed to add missing currencies: mocked add currency error: storage initialization failed",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockStorage := mock_cases.NewMockStorage(ctrl)
			mockScrapper := mock_cases.NewMockScrapper(ctrl)

			tc.mockBehavior(mockStorage, mockScrapper)

			mockLogger := logrus.New()
			cryptoService := &CryptoDetect{
				storage:  mockStorage,
				scrapper: mockScrapper,
				logger:   mockLogger,
			}

			err := cryptoService.ProcessMissingCoins(context.Background(), tc.coins)

			if tc.expectedErrorMessage != "" {
				assert.Error(t, err)
				assert.EqualError(t, err, tc.expectedErrorMessage)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}
