package cases

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	mock_cases "gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/cases/mocks"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/entities"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/pkg/logging"
)

func TestNewCryptoService(t *testing.T) {
	testTable := []struct {
		name                 string
		storage              Storage
		scrapper             Scrapper
		detect               CryptoDetect
		logger               *logrus.Logger
		expectedErrorMessage string
	}{
		{
			name:     "Valid parameters",
			storage:  &mock_cases.MockStorage{},
			scrapper: &mock_cases.MockScrapper{},
			detect:   &mock_cases.MockCryptoDetect{},
			logger:   logrus.New(),
		},
		{
			name:                 "Invalid Storage",
			storage:              nil,
			expectedErrorMessage: "new service storage init failid: <nil>: invalid param",
		},
		{
			name:                 "Invalid Scrapper",
			storage:              &mock_cases.MockStorage{},
			scrapper:             nil,
			expectedErrorMessage: "scrapper init failed: <nil>: invalid param",
		},
		{
			name:                 "Invalid Detect",
			storage:              &mock_cases.MockStorage{},
			scrapper:             &mock_cases.MockScrapper{},
			detect:               nil,
			expectedErrorMessage: "detect init failed: &{<nil> <nil>}: invalid param",
		},
		{
			name:                 "Invalid Logger",
			storage:              &mock_cases.MockStorage{},
			scrapper:             &mock_cases.MockScrapper{},
			detect:               &mock_cases.MockCryptoDetect{},
			logger:               nil,
			expectedErrorMessage: "logger init failed: <nil>: invalid param",
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			// Пытаемся создать новый CryptoService с заданными параметрами
			service, err := NewCryptoService(tt.storage, tt.scrapper, tt.detect, tt.logger)

			// Проверяем, соответствует ли результат ожиданиям
			if tt.expectedErrorMessage == "" {
				assert.NoError(t, err)
				assert.NotNil(t, service)
			} else {
				assert.Error(t, err)
				assert.Nil(t, service)
				assert.EqualError(t, err, tt.expectedErrorMessage)
			}
		})
	}
}

func TestGetRateByTitle(t *testing.T) {
	testTable := []struct {
		name                 string
		coins                []string
		expectedResponse     []*entities.Cryptocurrency
		mockBehavior         func(storage *mock_cases.MockStorage, scrapper *mock_cases.MockScrapper, detect *mock_cases.MockCryptoDetect)
		expectedErrorMessage string
	}{
		// Тесты для успешного получения курса валюты
		{
			name:             "Successful retrieval of rates",
			coins:            []string{"BTC"},
			expectedResponse: []*entities.Cryptocurrency{{Title: "Bitcoin", Price: 50000.0}},
			mockBehavior: func(storage *mock_cases.MockStorage, scrapper *mock_cases.MockScrapper, detect *mock_cases.MockCryptoDetect) {
				detect.EXPECT().ProcessMissingCoins(gomock.Any(), gomock.Any()).Return(nil)
				storage.EXPECT().GetCurrencies(gomock.Any()).Return([]string{"BTC"}, nil)
				storage.EXPECT().GetByCurrency(gomock.Any(), "BTC").Return(&entities.Cryptocurrency{Title: "Bitcoin", Price: 50000.0}, nil)

			},
			expectedErrorMessage: "",
		},
		{
			name:             "Errors process missing coins",
			coins:            []string{"BTC"},
			expectedResponse: nil,
			mockBehavior: func(storage *mock_cases.MockStorage, scrapper *mock_cases.MockScrapper, detect *mock_cases.MockCryptoDetect) {
				detect.EXPECT().ProcessMissingCoins(gomock.Any(), gomock.Any()).Return(errors.New("mocked saving error"))
			},
			expectedErrorMessage: "Failed to process missing coins: mocked saving error: storage initialization failed",
		},
		{
			name:             "Errors get currencies",
			coins:            []string{"BTC"},
			expectedResponse: nil,
			mockBehavior: func(storage *mock_cases.MockStorage, scrapper *mock_cases.MockScrapper, detect *mock_cases.MockCryptoDetect) {
				detect.EXPECT().ProcessMissingCoins(gomock.Any(), gomock.Any()).Return(nil)
				storage.EXPECT().GetCurrencies(gomock.Any()).Return(nil, errors.New("mocked saving error"))
			},
			expectedErrorMessage: "Failed to get coins: mocked saving error: storage initialization failed",
		},
		{
			name:             "Errors get by currency",
			coins:            []string{"BTC"},
			expectedResponse: nil,
			mockBehavior: func(storage *mock_cases.MockStorage, scrapper *mock_cases.MockScrapper, detect *mock_cases.MockCryptoDetect) {
				detect.EXPECT().ProcessMissingCoins(gomock.Any(), gomock.Any()).Return(nil)
				storage.EXPECT().GetCurrencies(gomock.Any()).Return([]string{"BTC"}, nil)
				storage.EXPECT().GetByCurrency(gomock.Any(), "BTC").Return(nil, errors.New("mocked saving error"))

			},
			expectedErrorMessage: "Failed to get rate for BTC: mocked saving error: storage initialization failed",
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockStorage := mock_cases.NewMockStorage(ctrl)
			mockScrapper := mock_cases.NewMockScrapper(ctrl)
			mockDetect := mock_cases.NewMockCryptoDetect(ctrl)
			logger := logging.GetLogger()

			// Вызываем mockBehavior, чтобы настроить поведение моков
			tt.mockBehavior(mockStorage, mockScrapper, mockDetect)

			// Создаем экземпляр CryptoService с моками
			server := &CryptoService{
				storage:  mockStorage,
				scrapper: mockScrapper,
				detect:   mockDetect,
				logger:   logger,
			}

			ctx := context.TODO()

			// Вызываем тестируемый метод
			result, err := server.GetRateByTitle(ctx, tt.coins)

			// Проверяем ожидаемые результаты
			if tt.expectedErrorMessage == "" {
				assert.NoError(t, err)
				assert.Equal(t, tt.expectedResponse, result)
			} else {
				assert.Error(t, err)
				assert.Equal(t, tt.expectedErrorMessage, err.Error())
			}
		})
	}
}

func TestGetStatsByTitle(t *testing.T) {
	testTable := []struct {
		name                 string
		coins                []string
		expectedResponse     []*entities.DailyChange
		mockBehavior         func(storage *mock_cases.MockStorage, scrapper *mock_cases.MockScrapper, detect *mock_cases.MockCryptoDetect)
		expectedErrorMessage string
	}{
		// Тесты для успешного получения курса валюты
		{
			name:             "Successful retrieval of rates",
			coins:            []string{"BTC"},
			expectedResponse: []*entities.DailyChange{{MaxValue: 55000.0, MinValue: 45000.0, PercentChange: 10.0}},
			mockBehavior: func(storage *mock_cases.MockStorage, scrapper *mock_cases.MockScrapper, detect *mock_cases.MockCryptoDetect) {
				detect.EXPECT().ProcessMissingCoins(gomock.Any(), gomock.Any()).Return(nil)
				storage.EXPECT().GetCurrencies(gomock.Any()).Return([]string{"BTC"}, nil)
				storage.EXPECT().GetStatsByTitle(gomock.Any(), "BTC").Return(&entities.DailyChange{MaxValue: 55000.0, MinValue: 45000.0, PercentChange: 10.0}, nil)

			},
			expectedErrorMessage: "",
		},
		{
			name:             "Errors process missing coins",
			coins:            []string{"BTC"},
			expectedResponse: nil,
			mockBehavior: func(storage *mock_cases.MockStorage, scrapper *mock_cases.MockScrapper, detect *mock_cases.MockCryptoDetect) {
				detect.EXPECT().ProcessMissingCoins(gomock.Any(), gomock.Any()).Return(errors.New("mocked saving error"))

			},
			expectedErrorMessage: "Failed to process missing coins: mocked saving error: storage initialization failed",
		},
		{
			name:             "Errors process missing coins",
			coins:            []string{"BTC"},
			expectedResponse: nil,
			mockBehavior: func(storage *mock_cases.MockStorage, scrapper *mock_cases.MockScrapper, detect *mock_cases.MockCryptoDetect) {
				detect.EXPECT().ProcessMissingCoins(gomock.Any(), gomock.Any()).Return(nil)
				storage.EXPECT().GetCurrencies(gomock.Any()).Return(nil, errors.New("mocked saving error"))
			},
			expectedErrorMessage: "Failed to get coins: mocked saving error: storage initialization failed",
		},
		{
			name:             "Errors get by currency",
			coins:            []string{"BTC"},
			expectedResponse: nil,
			mockBehavior: func(storage *mock_cases.MockStorage, scrapper *mock_cases.MockScrapper, detect *mock_cases.MockCryptoDetect) {

				detect.EXPECT().ProcessMissingCoins(gomock.Any(), gomock.Any()).Return(nil)
				storage.EXPECT().GetCurrencies(gomock.Any()).Return([]string{"BTC"}, nil)
				storage.EXPECT().GetStatsByTitle(gomock.Any(), "BTC").Return(nil, errors.New("mocked saving error"))

			},
			expectedErrorMessage: "failed to get details for BTC: mocked saving error: storage initialization failed",
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockStorage := mock_cases.NewMockStorage(ctrl)
			mockScrapper := mock_cases.NewMockScrapper(ctrl)
			mockDetect := mock_cases.NewMockCryptoDetect(ctrl)
			logger := logging.GetLogger()

			// Вызываем mockBehavior, чтобы настроить поведение моков
			tt.mockBehavior(mockStorage, mockScrapper, mockDetect)

			// Создаем экземпляр CryptoService с моками
			server := &CryptoService{
				storage:  mockStorage,
				scrapper: mockScrapper,
				detect:   mockDetect,
				logger:   logger,
			}

			ctx := context.TODO()

			// Вызываем тестируемый метод
			result, err := server.GetStatsByTitle(ctx, tt.coins)

			// Проверяем ожидаемые результаты
			if tt.expectedErrorMessage == "" {
				assert.NoError(t, err)
				assert.Equal(t, tt.expectedResponse, result)
			} else {
				assert.Error(t, err)
				assert.Equal(t, tt.expectedErrorMessage, err.Error())
			}
		})
	}
}

func TestGetActualRate(t *testing.T) {
	testTable := []struct {
		name                 string
		mockBehavior         func(scrapper *mock_cases.MockScrapper, storage *mock_cases.MockStorage)
		expectedErrorMessage string
	}{
		{
			name: "Successful retrieval and saving of cryptocurrency data",
			mockBehavior: func(scrapper *mock_cases.MockScrapper, storage *mock_cases.MockStorage) {
				storage.EXPECT().GetCurrencies(gomock.Any()).Return([]string{"BTC"}, nil)
				scrapper.EXPECT().GetCourse(gomock.Any(), "BTC").Return(&entities.Cryptocurrency{Title: "BTC", Price: 50000.0}, nil)
				storage.EXPECT().Save(gomock.Any(), gomock.Any()).Return(nil)
			},
			expectedErrorMessage: "",
		},
		{
			name: "Error get cryptocurrencies",
			mockBehavior: func(scrapper *mock_cases.MockScrapper, storage *mock_cases.MockStorage) {
				storage.EXPECT().GetCurrencies(gomock.Any()).Return(nil, errors.New("mocked saving error"))
			},
			expectedErrorMessage: "Failed to get coins: mocked saving error: storage initialization failed",
		},
		{
			name: "Error save cryptocurrency data",
			mockBehavior: func(scrapper *mock_cases.MockScrapper, storage *mock_cases.MockStorage) {
				storage.EXPECT().GetCurrencies(gomock.Any()).Return([]string{"BTC"}, nil)
				scrapper.EXPECT().GetCourse(gomock.Any(), "BTC").Return(&entities.Cryptocurrency{Title: "BTC", Price: 50000.0}, nil)
				storage.EXPECT().Save(gomock.Any(), gomock.Any()).Return(errors.New("mocked error"))
			},
			expectedErrorMessage: "Failed to save data: mocked error: storage initialization failed",
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()

			mockScrapper := mock_cases.NewMockScrapper(mockCtrl)
			mockStorage := mock_cases.NewMockStorage(mockCtrl)
			logger := logging.GetLogger()
			tt.mockBehavior(mockScrapper, mockStorage)

			cryptoService := &CryptoService{
				scrapper: mockScrapper,
				storage:  mockStorage,
				logger:   logger,
			}

			err := cryptoService.GetActualRate(context.Background())

			if tt.expectedErrorMessage == "" {
				assert.NoError(t, err)
			} else {
				assert.Error(t, err)
				assert.Equal(t, tt.expectedErrorMessage, err.Error())
			}
		})
	}
}
