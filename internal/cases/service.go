package cases

import "context"

//go:generate go run github.com/golang/mock/mockgen -source=service.go -destination=mocks/service_mock.go
type CryptoServiceInterface interface {
	ProcessMissingCoins(ctx context.Context, coins []string) error
}
