package cases

import (
	"context"

	"gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/entities"
)

//go:generate go run github.com/golang/mock/mockgen -source=storage.go -destination=mocks/storage_mock.go
type Storage interface {
	Save(ctx context.Context, rates []*entities.Cryptocurrency) error
	GetByCurrency(ctx context.Context, currency string) (*entities.Cryptocurrency, error)
	GetStatsByTitle(ctx context.Context, currency string) (*entities.DailyChange, error)
	AddCurrency(ctx context.Context, currency []string) error
	GetCurrencies(ctx context.Context) ([]string, error)
}
