package cases

import "context"

//go:generate go run github.com/golang/mock/mockgen -source=crypto_detect.go -destination=mocks/detect_mock.go
type CryptoDetect interface {
	ProcessMissingCoins(ctx context.Context, coins []string) error
}
