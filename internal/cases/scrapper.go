package cases

import (
	"context"

	"gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/entities"
)

//go:generate go run github.com/golang/mock/mockgen -source=scrapper.go -destination=mocks/scrapper_mock.go

type Scrapper interface {
	GetURLScrapper(ctx context.Context, currencies string) (string, error)
	GetCourse(ctx context.Context, currencies string) (*entities.Cryptocurrency, error)
}
