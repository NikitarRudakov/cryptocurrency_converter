package cases

import (
	"context"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/entities"
)

type CryptoService struct {
	storage  Storage
	scrapper Scrapper
	detect   CryptoDetect
	logger   *logrus.Logger
}

func NewCryptoService(storage Storage, scrapper Scrapper, detect CryptoDetect, logger *logrus.Logger) (*CryptoService, error) {
	if storage == nil || storage == Storage(nil) {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "new service storage init failid: %v", storage)
	}
	if scrapper == nil || scrapper == Scrapper(nil) {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "scrapper init failed: %v", scrapper)
	}
	if detect == nil || detect == CryptoDetect(nil) {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "detect init failed: %v", scrapper)
	}
	if logger == nil {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "logger init failed: %v", logger)
	}
	return &CryptoService{
		storage:  storage,
		scrapper: scrapper,
		detect:   detect,
		logger:   logger,
	}, nil
}

func (s *CryptoService) GetRateByTitle(ctx context.Context, coins []string) ([]*entities.Cryptocurrency, error) {
	errMissingCoins := s.detect.ProcessMissingCoins(ctx, coins)
	if errMissingCoins != nil {
		s.logger.Errorf("Failed to process missing coins: %v", errMissingCoins)
		return nil, errors.Wrapf(entities.ErrStorageInitFail, "Failed to process missing coins: %v", errMissingCoins)
	}
	cryptoCoins, errGetCoins := s.storage.GetCurrencies(ctx)
	if errGetCoins != nil {
		s.logger.Errorf("Failed to get coins: %v", errGetCoins)
		return nil, errors.Wrapf(entities.ErrStorageInitFail, "Failed to get coins: %v", errGetCoins)
	}

	// Создаем карту для хранения валют, которые уже есть в cryptoCoins
	existingCryptoCoins := make(map[string]struct{})
	for _, v := range cryptoCoins {
		existingCryptoCoins[v] = struct{}{}
	}

	// Создаем срез для хранения валют, которые присутствуют и в cryptoCoins, и в coins
	commonCryptoCoins := []string{}
	for _, v := range coins {
		// Проверяем, есть ли текущая валюта в списке уже существующих в cryptoCoins
		if _, ok := existingCryptoCoins[v]; ok {
			commonCryptoCoins = append(commonCryptoCoins, v)
		}
	}
	var rates []*entities.Cryptocurrency
	for _, currency := range commonCryptoCoins {
		rate, err := s.storage.GetByCurrency(ctx, currency)
		if err != nil {
			s.logger.Errorf("Failed to get rate for %s: %v", currency, err)
			return nil, errors.Wrapf(entities.ErrStorageInitFail, "Failed to get rate for %s: %v", currency, err)
		}
		crypto := &entities.Cryptocurrency{
			Title:        rate.Title,
			Price:        rate.Price,
			LastViewTime: rate.LastViewTime,
		}
		rates = append(rates, crypto)
	}
	return rates, nil
}

func (s *CryptoService) GetStatsByTitle(ctx context.Context, coins []string) ([]*entities.DailyChange, error) {
	errMissingCoins := s.detect.ProcessMissingCoins(ctx, coins)
	if errMissingCoins != nil {
		s.logger.Errorf("Failed to process missing coins: %v", errMissingCoins)
		return nil, errors.Wrapf(entities.ErrStorageInitFail, "Failed to process missing coins: %v", errMissingCoins)
	}
	cryptoCoins, errGetCoins := s.storage.GetCurrencies(ctx)
	if errGetCoins != nil {
		s.logger.Errorf("Failed to get coins: %v", errGetCoins)
		return nil, errors.Wrapf(entities.ErrStorageInitFail, "Failed to get coins: %v", errGetCoins)
	}

	// Создаем карту для хранения валют, которые уже есть в cryptoCoins
	existingCryptoCoins := make(map[string]struct{})
	for _, v := range cryptoCoins {
		existingCryptoCoins[v] = struct{}{}
	}

	// Создаем срез для хранения валют, которые присутствуют и в cryptoCoins, и в coins
	commonCryptoCoins := []string{}
	for _, v := range coins {
		// Проверяем, есть ли текущая валюта в списке уже существующих в cryptoCoins
		if _, ok := existingCryptoCoins[v]; ok {
			commonCryptoCoins = append(commonCryptoCoins, v)
		}
	}

	var changes []*entities.DailyChange
	for _, currency := range commonCryptoCoins {
		// Получаем статистику по криптовалюте из хранилища
		stats, err := s.storage.GetStatsByTitle(ctx, currency)
		if err != nil {
			// Если произошла ошибка, логируем ее и возвращаем
			s.logger.Errorf("failed to get details for %s: %v", currency, err)
			return nil, errors.Wrapf(entities.ErrStorageInitFail, "failed to get details for %s: %v", currency, err)
		}

		// Создаем новый объект DailyChange с полученными данными
		change := &entities.DailyChange{
			Currency:      stats.Currency,
			MaxValue:      stats.MaxValue,
			MinValue:      stats.MinValue,
			PercentChange: stats.PercentChange,
		}

		// Добавляем объект в список изменений
		changes = append(changes, change)
	}

	// Возвращаем список изменений
	return changes, nil
}

func (s *CryptoService) GetActualRate(ctx context.Context) error {
	// Получаем список валют
	coins, errGetCoin := s.storage.GetCurrencies(ctx)
	if errGetCoin != nil {
		// Логируем ошибку и возвращаем
		s.logger.Errorf("Failed to get coins: %v", errGetCoin)
		return errors.Wrapf(entities.ErrStorageInitFail, "Failed to get coins: %v", errGetCoin)
	}

	// Инициализируем слайс для хранения результатов
	var cryptocurrencies []*entities.Cryptocurrency

	// Получаем актуальные курсы для каждой валюты
	for _, coin := range coins {
		result, errCoins := s.scrapper.GetCourse(ctx, coin)
		if errCoins != nil {
			// Логируем ошибку и переходим к следующей валюте
			s.logger.Errorf("Failed to get cryptocurrency: %v", errCoins)
			continue
		}
		cryptocurrencies = append(cryptocurrencies, result)
	}

	// Сохраняем результаты
	errSave := s.storage.Save(ctx, cryptocurrencies)
	if errSave != nil {
		// Логируем ошибку и возвращаем
		s.logger.Errorf("Failed to save data: %v", errSave)
		return errors.Wrapf(entities.ErrStorageInitFail, "Failed to save data: %v", errSave)
	}
	return nil
}
