package ports

/*func TestNewServer(t *testing.T) {
	testTable := []struct {
		name                 string
		service              *mock_ports.MockService
		expectedErrorMessage string
	}{
		{
			name:    "Valid parameters",
			service: &mock_ports.MockService{},
		},
		{
			name:    "Invalid Service",
			service: nil,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			// Пытаемся создать новый ScrapingService с заданными параметрами
			service, err := NewServer(tt.service)

			// Проверяем, соответствует ли результат ожиданиям
			if tt.expectedErrorMessage == "" {
				assert.NoError(t, err)
				assert.NotNil(t, service)
			} else {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), tt.expectedErrorMessage)
				assert.Nil(t, service)
			}
		})
	}
}

func TestHandleGetRates(t *testing.T) {
	testTable := []struct {
		name                 string
		expectedResponse     string
		mockBehavior         func(service *mock_ports.MockService)
		expectedErrorMessage string
	}{
		{
			name:             "Ok",
			expectedResponse: "mocked cryptocurrency rates",
			mockBehavior: func(service *mock_ports.MockService) {
				service.EXPECT().GetRates(gomock.Any()).Return("mocked cryptocurrency rates", nil)
			},
			expectedErrorMessage: "",
		},
		{
			name:             "Error GetRates",
			expectedResponse: "",
			mockBehavior: func(service *mock_ports.MockService) {
				service.EXPECT().GetRates(gomock.Any()).Return("", errors.New("mocked error"))
			},
			expectedErrorMessage: "failed to get cryptocurrency rates: mocked error",
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockService := mock_ports.NewMockService(ctrl)
			server := &Server{service: mockService}

			ctx := context.TODO()

			if tt.mockBehavior != nil {
				tt.mockBehavior(mockService)
			}

			response, err := server.HandleGetRates(ctx)

			if tt.expectedErrorMessage == "" {
				assert.NoError(t, err)
				assert.Equal(t, tt.expectedResponse, response)
			} else {
				assert.Error(t, err)
				assert.Equal(t, tt.expectedErrorMessage, err.Error())
			}
		})
	}
}

func TestHandleGetRateByCurrency(t *testing.T) {
	testTable := []struct {
		name                 string
		currency             string
		expectedResponse     string
		mockBehavior         func(service *mock_ports.MockService)
		expectedErrorMessage string
	}{
		{
			name:             "Ok",
			currency:         "BTC",
			expectedResponse: "mocked cryptocurrency rate for BTC",
			mockBehavior: func(service *mock_ports.MockService) {
				service.EXPECT().GetRateByTitle(gomock.Any(), "BTC").Return("mocked cryptocurrency rate for BTC", nil)
			},
			expectedErrorMessage: "",
		},
		{
			name:             "Error GetRateByTitle",
			currency:         "ETH",
			expectedResponse: "",
			mockBehavior: func(service *mock_ports.MockService) {
				service.EXPECT().GetRateByTitle(gomock.Any(), "ETH").Return("", errors.New("mocked error"))
			},
			expectedErrorMessage: "failed to get cryptocurrency rate for ETH: mocked error",
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockService := mock_ports.NewMockService(ctrl)
			server := &Server{service: mockService}

			ctx := context.TODO()

			if tt.mockBehavior != nil {
				tt.mockBehavior(mockService)
			}

			response, err := server.HandleGetRateByCurrency(ctx, tt.currency)

			if tt.expectedErrorMessage == "" {
				assert.NoError(t, err)
				assert.Equal(t, tt.expectedResponse, response)
			} else {
				assert.Error(t, err)
				assert.Equal(t, tt.expectedErrorMessage, err.Error())
			}
		})
	}
}

func TestHandleGetDetails(t *testing.T) {
	testTable := []struct {
		name                 string
		currency             string
		expectedResponse     string
		mockBehavior         func(service *mock_ports.MockService)
		expectedErrorMessage string
	}{
		{
			name:             "Ok",
			currency:         "BTC",
			expectedResponse: "mocked details for BTC",
			mockBehavior: func(service *mock_ports.MockService) {
				service.EXPECT().GetDetails(gomock.Any(), "BTC").Return("mocked details for BTC", nil)
			},
			expectedErrorMessage: "",
		},
		{
			name:             "Error GetDetails",
			currency:         "ETH",
			expectedResponse: "",
			mockBehavior: func(service *mock_ports.MockService) {
				service.EXPECT().GetDetails(gomock.Any(), "ETH").Return("", errors.New("mocked error"))
			},
			expectedErrorMessage: "failed to get details for ETH: mocked error",
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockService := mock_ports.NewMockService(ctrl)
			server := &Server{service: mockService}

			ctx := context.TODO()

			if tt.mockBehavior != nil {
				tt.mockBehavior(mockService)
			}

			response, err := server.HandleGetDetails(ctx, tt.currency)

			if tt.expectedErrorMessage == "" {
				assert.NoError(t, err)
				assert.Equal(t, tt.expectedResponse, response)
			} else {
				assert.Error(t, err)
				assert.Equal(t, tt.expectedErrorMessage, err.Error())
			}
		})
	}
}
*/
