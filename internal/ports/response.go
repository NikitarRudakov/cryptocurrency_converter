package ports

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type ErrorResponse struct {
	Message string `json:"message"`
}

func newErrorResponse(c *gin.Context, statusCode int, message string, logger *logrus.Logger) {
	logger.Error(message)
	c.AbortWithStatusJSON(statusCode, ErrorResponse{Message: message})
}
