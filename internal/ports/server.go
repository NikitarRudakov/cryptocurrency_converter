package ports

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/dto"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/entities"
)

type Server struct {
	service    Service
	httpServer *http.Server
	logger     *logrus.Logger
}

// NewServer конструктор для создания нового экземпляра Server
func NewServer(service Service, logger *logrus.Logger) (*Server, error) {
	if service == nil || service == Service(nil) {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "new server init failed: %v", service)
	}

	if logger == nil {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "logger init failed: %v", logger)
	}

	return &Server{
		service: service,
		logger:  logger,
	}, nil
}

func (s *Server) RunServer(port string, router *gin.Engine) error {
	s.httpServer = &http.Server{
		Addr:           ":" + port,
		Handler:        router,
		MaxHeaderBytes: 1 << 20,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
	}
	s.logger.Infof("Server is running on port %s", port)
	return s.httpServer.ListenAndServe()
}

func (s *Server) Shutdown(ctx context.Context) error {
	return s.httpServer.Shutdown(ctx)
}

// GetRateByTitle godoc
// @Summary Get currency rate by title
// @Tags Currency
// @Description Retrieves the rate of the specified cryptocurrency.
// @Accept json
// @Produce json
// @Param coin body dto.CoinsRequest true "Cryptocurrency name to get the rate"
// @Success 200 {object} dto.RatesResponse "Successful response"
// @Failure default {object} ErrorResponse
// @Router /api/crypto/{rates} [get]
func (s *Server) GetRateByTitle(c *gin.Context) {
	s.logger.Info("Received request to get rate by title")

	// Получение списка валют из запроса
	var request dto.CoinsRequest
	if err := c.BindJSON(&request); err != nil {
		s.logger.Error("Error binding JSON: ", err)
		newErrorResponse(c, http.StatusBadRequest, err.Error(), s.logger)
		return
	}
	fmt.Println(request.Coin)
	// Получаем курс валюты
	rates, err := s.service.GetRateByTitle(c, request.Coin)
	if err != nil {
		s.logger.Error("Error getting rate by title: ", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Создаем список для ответа о курсах и ошибки
	var responseRates []*dto.Rates
	// Создаем карту для хранения сообщений об ошибках
	errors := make(map[string]string)
	// Проходимся по каждой валюте в запросе
	for _, coin := range request.Coin {
		fmt.Println("Валюта отправленная пользователем", coin)
		found := false
		for _, rate := range rates {
			fmt.Println("Валюта на сервере", rate.Title)
			if rate.Title == coin {
				// Если нашли курс для текущей валюты, добавляем его в список
				responseRates = append(responseRates, &dto.Rates{
					Title: rate.Title,
					Price: rate.Price,
				})
				found = true
				fmt.Println("Курс найден", rate.Title, rate.Price)
				break
			}
		}
		// Если не нашли курс для текущей валюты, добавляем сообщение об ошибке
		if !found {
			errors[coin] = fmt.Sprintf("The coin %s does not exist", coin)
		}
	}

	// Формируем ответ
	response := make(map[string]interface{})
	if len(responseRates) > 0 {
		response["rates"] = responseRates
	}
	if len(errors) > 0 {
		response["errors"] = errors
	}

	// Отправляем ответ
	c.JSON(http.StatusOK, response)
}

// GetStatsByTitle godoc
// @Summary Get statistics by currency name
// @Tags Currency
// @Description Retrieves statistics for the specified cryptocurrency.
// @Accept json
// @Produce json
// @Param coin body dto.CoinsRequest true "Cryptocurrency name to get statistics"
// @Success 200 {object} dto.StatsResponse "Successful response"
// @Failure default {object} ErrorResponse
// @Router /api/crypto/{stats} [get]
func (s *Server) GetStatsByTitle(c *gin.Context) {
	s.logger.Info("Received request to get stats by title")

	// Получение списка валют из запроса
	var request dto.CoinsRequest
	if err := c.BindJSON(&request); err != nil {
		s.logger.Error("Error binding JSON: ", err)
		newErrorResponse(c, http.StatusBadRequest, err.Error(), s.logger)
		return
	}

	// Получаем статистику по валютам
	stats, err := s.service.GetStatsByTitle(c, request.Coin)
	if err != nil {
		s.logger.Error("Error getting stats by title: ", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Создаем список для ответа о статистике и ошибки
	var statsResponse []*dto.Stats
	// Создаем карту для хранения сообщений об ошибках
	errors := make(map[string]string)
	// Проходимся по каждой валюте в запросе
	for _, coin := range request.Coin {
		found := false
		// Проходимся по каждой статистике
		for _, stat := range stats {
			if stat.Currency == coin {
				// Если нашли статистику для текущей валюты, добавляем ее в список
				percentChangeFormatted := fmt.Sprintf("%.2f", stat.PercentChange)
				statsResponse = append(statsResponse, &dto.Stats{
					Title:         stat.Currency,
					MaxValue:      stat.MaxValue,
					MinValue:      stat.MinValue,
					PercentChange: percentChangeFormatted,
				})
				found = true
				break
			}
		}
		// Если не нашли статистику для текущей валюты, добавляем сообщение об ошибке
		if !found {
			errors[coin] = fmt.Sprintf("The coin %s does not exist", coin)
		}
	}

	// Формируем ответ
	response := make(map[string]interface{})
	if len(statsResponse) > 0 {
		response["stats"] = statsResponse
	}
	if len(errors) > 0 {
		response["errors"] = errors
	}

	// Отправляем ответ
	c.JSON(http.StatusOK, response)
}

func (s *Server) RunTicker(done <-chan struct{}) {
	s.logger.Info("Ticker started")

	if err := s.service.GetActualRate(context.Background()); err != nil {
		s.logger.Error("Error occurred during initial rate update: ", err)
	} else {
		s.logger.Info("Initial actual rate updated successfully")
	}

	ticker := time.NewTicker(5 * time.Minute)
	defer ticker.Stop()

	for {
		select {
		case <-done:
			s.logger.Info("Ticker stopped")
			return
		case <-ticker.C:
			err := s.service.GetActualRate(context.Background())
			if err != nil {
				s.logger.Error("Error occurred: ", err)
			} else {
				s.logger.Info("Actual rate updated successfully")
			}
		}
	}
}
