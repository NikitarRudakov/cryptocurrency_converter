package ports

import (
	"context"

	"gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/entities"
)

//go:generate go run github.com/golang/mock/mockgen -source=crypto_service.go -destination=mocks/crypto_service_mock.go
type Service interface {
	GetRateByTitle(ctx context.Context, coins []string) ([]*entities.Cryptocurrency, error)
	GetStatsByTitle(ctx context.Context, coins []string) ([]*entities.DailyChange, error)
	GetActualRate(ctx context.Context) error
}
