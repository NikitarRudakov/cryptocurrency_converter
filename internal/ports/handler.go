package ports

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/NikitarRudakov/cryptocurrency_converter/docs"
)

func (s *Server) InitRoutes() *gin.Engine {
	router := gin.New()

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	api := router.Group("/api")
	{
		crypto := api.Group("/crypto")
		{
			crypto.GET("/{rates}", s.GetRateByTitle)
			crypto.GET("/{stats}", s.GetStatsByTitle)
		}
	}

	return router
}
