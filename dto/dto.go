package dto

type CoinsRequest struct {
	Coin []string `json:"coin" binding:"required"`
}

type Rates struct {
	Title string  `json:"title"`
	Price float64 `json:"price"`
}

type RatesResponse struct {
	Rates []*Rates `json:"rates"`
}

type Stats struct {
	Title         string  `json:"title"`
	MaxValue      float64 `json:"max_value"`
	MinValue      float64 `json:"min_value"`
	PercentChange string  `json:"percent_change"`
}

type StatsResponse struct {
	Stats []*Stats `json:"stats"`
}
