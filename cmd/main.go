package main

import (
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/cmd/app"
)

func main() {
	app.Start()
}
