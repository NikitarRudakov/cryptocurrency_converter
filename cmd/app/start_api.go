package app

import (
	"context"

	"gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/adapters"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/cases"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/internal/ports"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/pkg/configs"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/pkg/db/postgresql"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/pkg/logging"
)

// @title           Cryptocurrency Converter API
// @version         1.0
// @description     This is an example of a cryptocurrency conversion API server. The API provides methods for obtaining the current exchange rate between different cryptocurrencies.

// @host      localhost:8080
// @BasePath  /

func Start() {
	logger := logging.GetLogger()
	cfg, err := configs.GetConfig()
	if err != nil {
		logger.Fatalf("Error getting config: %v", err)
	}
	postgreSQLClient, err := postgresql.NewClient(context.TODO(), 3, cfg)
	if err != nil {
		logger.Fatalf("Error creating PostgreSQL client: %v", err)
	}
	pgStorage, err := adapters.NewPGStorage(postgreSQLClient, logger)
	if err != nil {
		logger.Fatalf("Error creating PGStorage: %v\n", err)
	}
	scrapperService, err := adapters.NewScrapperCryptoCompare(logger)
	if err != nil {
		logger.Fatalf("Error creating ScrapperService: %v\n", err)
	}
	cryptoDetect, err := adapters.NewCryptoDetect(pgStorage, scrapperService, logger)
	if err != nil {
		logger.Fatalf("Error creating CryptoDetect: %v\n", err)
	}
	cryptoService, err := cases.NewCryptoService(pgStorage, scrapperService, cryptoDetect, logger)
	if err != nil {
		logger.Fatalf("Error creating CryptoService: %v\n", err)
	}

	srv, err := ports.NewServer(cryptoService, logger)
	if err != nil {
		logger.Fatalf("Error creating Server: %v\n", err)
	}
	done := make(chan struct{})
	defer close(done)

	go srv.RunTicker(done)
	if err := srv.RunServer("8080", srv.InitRoutes()); err != nil {
		logger.Fatalf("error occured while running http server: %s", err.Error())
	}
}
