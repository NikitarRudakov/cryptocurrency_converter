// Package docs Code generated by swaggo/swag. DO NOT EDIT
package docs

import "github.com/swaggo/swag"

const docTemplate = `{
    "schemes": {{ marshal .Schemes }},
    "swagger": "2.0",
    "info": {
        "description": "{{escape .Description}}",
        "title": "{{.Title}}",
        "contact": {},
        "version": "{{.Version}}"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/api/crypto/{rates}": {
            "get": {
                "description": "Retrieves the rate of the specified cryptocurrency.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Currency"
                ],
                "summary": "Get currency rate by title",
                "parameters": [
                    {
                        "description": "Cryptocurrency name to get the rate",
                        "name": "coin",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/gitlab_com_NikitarRudakov_cryptocurrency_converter_internal_ports_dto.CoinsRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful response",
                        "schema": {
                            "$ref": "#/definitions/gitlab_com_NikitarRudakov_cryptocurrency_converter_internal_ports_dto.RatesResponse"
                        }
                    },
                    "default": {
                        "description": "",
                        "schema": {
                            "$ref": "#/definitions/cryptocurrency_converter_internal_ports.ErrorResponse"
                        }
                    }
                }
            }
        },
        "/api/crypto/{stats}": {
            "get": {
                "description": "Retrieves statistics for the specified cryptocurrency.",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Currency"
                ],
                "summary": "Get statistics by currency name",
                "parameters": [
                    {
                        "description": "Cryptocurrency name to get statistics",
                        "name": "coin",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/gitlab_com_NikitarRudakov_cryptocurrency_converter_internal_ports_dto.CoinsRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful response",
                        "schema": {
                            "$ref": "#/definitions/gitlab_com_NikitarRudakov_cryptocurrency_converter_internal_ports_dto.StatsResponse"
                        }
                    },
                    "default": {
                        "description": "",
                        "schema": {
                            "$ref": "#/definitions/cryptocurrency_converter_internal_ports.ErrorResponse"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "cryptocurrency_converter_internal_ports.ErrorResponse": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string"
                }
            }
        },
        "gitlab_com_NikitarRudakov_cryptocurrency_converter_internal_ports_dto.CoinsRequest": {
            "type": "object",
            "required": [
                "coin"
            ],
            "properties": {
                "coin": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                }
            }
        },
        "gitlab_com_NikitarRudakov_cryptocurrency_converter_internal_ports_dto.Rates": {
            "type": "object",
            "properties": {
                "price": {
                    "type": "number"
                },
                "title": {
                    "type": "string"
                }
            }
        },
        "gitlab_com_NikitarRudakov_cryptocurrency_converter_internal_ports_dto.RatesResponse": {
            "type": "object",
            "properties": {
                "rates": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/gitlab_com_NikitarRudakov_cryptocurrency_converter_internal_ports_dto.Rates"
                    }
                }
            }
        },
        "gitlab_com_NikitarRudakov_cryptocurrency_converter_internal_ports_dto.Stats": {
            "type": "object",
            "properties": {
                "max_value": {
                    "type": "number"
                },
                "min_value": {
                    "type": "number"
                },
                "percent_change": {
                    "type": "string"
                },
                "title": {
                    "type": "string"
                }
            }
        },
        "gitlab_com_NikitarRudakov_cryptocurrency_converter_internal_ports_dto.StatsResponse": {
            "type": "object",
            "properties": {
                "stats": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/gitlab_com_NikitarRudakov_cryptocurrency_converter_internal_ports_dto.Stats"
                    }
                }
            }
        }
    }
}`

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo = &swag.Spec{
	Version:          "1.0",
	Host:             "localhost:8080",
	BasePath:         "/",
	Schemes:          []string{},
	Title:            "Cryptocurrency Converter API",
	Description:      "This is an example of a cryptocurrency conversion API server. The API provides methods for obtaining the current exchange rate between different cryptocurrencies.",
	InfoInstanceName: "swagger",
	SwaggerTemplate:  docTemplate,
	LeftDelim:        "{{",
	RightDelim:       "}}",
}

func init() {
	swag.Register(SwaggerInfo.InstanceName(), SwaggerInfo)
}
